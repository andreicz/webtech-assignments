
const FIRST_NAME = "Andrei";
const LAST_NAME = "Cazacu";
const GRUPA = "1090";

/**
 * Make the implementation here
 */
function initCaching() {
   var cache = {};
   var cacheValue = {};
   cache.pageAccessCounter = function(value = "home") {
        value = value.toLowerCase();
        if(cacheValue[value] === undefined)
            cacheValue[value] = 1;
        else
            cacheValue[value]++;
   };
   cache.getCache = function() {
        return cacheValue;
   };

   return cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}